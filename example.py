# coding=utf-8

"""
Kinetic Performance Technology PTY LTD produces the GymAware Cloud platform
to support our GymAware Powertool.

This is a simple example of using the GymAware API to access an accounts' data.

Follow the steps in https://gymaware.zendesk.com/hc/en-us/articles/360001396875-API-Integration for
information on obtaining access to your account.
"""

from __future__ import unicode_literals
from __future__ import print_function

import argparse
import json
import time
import datetime
from collections import defaultdict
import httpx


PROTOCOL = 'https'
DOMAIN = 'gymaware.kinetic.com.au'
GYMAWARE_URL = f"{PROTOCOL}://{DOMAIN}/api/"

NO_DATA = "No results were found on the server."


def server_url(account_id, account_token):
    return '{protocol}://{account_id}:{account_token}@{domain}/api/'.format(
        protocol=PROTOCOL,
        account_id=account_id,
        account_token=account_token,
        domain=DOMAIN,
    )


server = httpx.Client(http2=True)

request_start = datetime.datetime.now() - datetime.timedelta(days=30)


def print_date_time(gmt_time):
    """
    Take a gmt time and format it in iso format
    :param gmt_time: unix timestamp
    :return: str
    """
    return datetime.datetime.fromtimestamp(gmt_time).strftime('%Y-%m-%dT%H:%M:%S')


def summaries_from_server(authentication, days=30, athleteReference=None):
    """
    Fetch a summary of each GymAware Sets updated in the last 30 days.
    :param authentication: Account Key, API token tuple.
    :param days: Limit the request to data collected in the last N days.
    """
    start = datetime.datetime.now() - datetime.timedelta(days=days)
    params = {'modifiedSince': time.mktime(start.utctimetuple())}
    if athleteReference:
        params['athleteReference'] = athleteReference
    with server.stream(
            "GET",
            GYMAWARE_URL + 'summaries',
            auth=authentication,
            params=params
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            yield json.loads(line)


def bests_from_server(authentication):
    """
    Fetch the best results for all users from this year.
    :param authentication: Account Key, API token tuple.
    """
    with server.stream(
        "GET",
        GYMAWARE_URL+'bests',
        auth=authentication,
        params={
            'start': time.mktime(request_start.utctimetuple()),
            'end': time.mktime((request_start + datetime.timedelta(days=30)).utctimetuple()),
        },
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            yield json.loads(line)


def reps_from_server(authentication, athleteReference=None):
    """
    Fetch the rep results for all users from this year.
    :param authentication: Account Key, API token tuple.
    """
    params = {
        'start': time.mktime(request_start.utctimetuple()),
        'end': time.time()
    }
    if athleteReference:
        params['athleteReference'] = athleteReference
    with server.stream(
        'GET',
        GYMAWARE_URL+'reps',
        auth=authentication,
        params=params,
        timeout=None
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            yield json.loads(line)


def analysis_from_server(authentication):
    """
    Fetch the analysis types for rep results.
    The rep results will be keyed with these labels.
    :param authentication: Account Key, API token tuple.
    """
    with server.stream(
        'GET',
        GYMAWARE_URL+'analysis',
        auth=authentication
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            yield json.loads(line)


def exercises_from_server(authentication):
    """
    Fetch the available exercise list for this account.
    :param authentication: Account Key, API token tuple.
    """
    with server.stream(
            "GET",
            GYMAWARE_URL + 'exercises',
            auth=authentication,
    ) as request:
        request.raise_for_status()
        for line in request.iter_lines():
            yield json.loads(line)


def athletes_from_server(authentication):
    """
    Fetch dictionaries of athlete information.
    :param authentication: key and token
    """
    with server.stream("GET", GYMAWARE_URL + 'athletes', auth=authentication) as request:
        request.raise_for_status()
        for line in request.iter_lines():
            yield json.loads(line)


def staff_from_server(authentication):
    """
    Fetch dictionaries of staff information.
    :param authentication: key and token
    """
    with server.stream("GET", GYMAWARE_URL + 'staff', auth=authentication) as request:
        request.raise_for_status()
        for line in request.iter_lines():
            yield json.loads(line)


def squads_from_server(authentication, staffReference):
    """
    Fetch dictionaries of squad information.
    :param authentication: key and token
    """
    with server.stream("GET", GYMAWARE_URL + 'squad',
                       params={'staffReference': staffReference},
                       auth=authentication) as request:
        request.raise_for_status()
        for line in request.iter_lines():
            yield json.loads(line)


def velocity_zone_results(authentication, athleteReference=None):
    """
    Example of using the summaries endpoint to get the velocity zones
    of all the latest GymAware sets.
    :param authentication: Account Key, API token tuple.
    """
    by_zone = defaultdict(list)
    for summary in summaries_from_server(authentication, athleteReference=athleteReference):
        by_zone[summary['velocityZone']].append(
            (summary['athleteName'], summary['meanVelocity'], summary['notes'])
        )
    for zone_name, zone_result in by_zone.items():
        yield zone_name, sorted(
            zone_result,
            key=lambda user_v: user_v[1],
            reverse=True
        )


def sorted_best_powers(authentication):
    """
    Use the bests endpoint to get every athletes best meanPower and rank them.
    :param authentication: Account Key, API token tuple.
    """
    best_powers = {}
    for best in bests_from_server(authentication):
        best_powers[best['athleteName']] = best['barWeight']
    return sorted(
        best_powers.items(),
        key=lambda user_power: user_power[1], reverse=True
    )


def analysis_by_label(authentication):
    return {
        analysis['label']: analysis
        for analysis in analysis_from_server(authentication)
    }


def exercises(authentication):
    return sorted(
        "{category} : {name} ({subname}) : {reference}".format(**exercise)
        for exercise in exercises_from_server(authentication)
    )


def refresh_credentials_from_server(authentication):
    response = server.post(
        GYMAWARE_URL+'refresh',
        auth=authentication,
    )
    response.raise_for_status()
    return response.json()


def rep_stats(authentication, analysis, athleteReference=None):
    """
    Use the reps endpoint to get every athletes' analysis.
    :param authentication: Account Key, API token tuple.
    :param analysis: dict describing how to use the analysis type.
    """
    if analysis['isMin']:
        func = min
    elif analysis['isMean']:
        def func(x):
            return sum(x)/len(x)
    else:
        func = max
    results = {}
    for session in reps_from_server(authentication, athleteReference):
        results[
            (session['athleteName'], print_date_time(session['recorded']))
        ] = func([rep[analysis['label']] for rep in session['reps']])
    return sorted(results.items())


def add_athlete(authentication, athlete_parameters):
    """
    Example of adding a new athlete into the GymAware system.
    :param authentication: Account Key, API token tuple.
    :param athlete_parameters:
        Iterable of (first name, last name) for an athlete.
    """
    athlete_parameters = {
        key: value for key, value in
        zip(('firstName', 'lastName'), athlete_parameters)
    }
    athlete_request = server.post(
        GYMAWARE_URL + 'athletes',
        auth=authentication,
        data=athlete_parameters
    )
    if athlete_request.status_code == 409:
        raise Exception(athlete_request.headers.get('Exception'))
    else:
        athlete_request.raise_for_status()
    return athlete_request.json()


def print_demo():
    """
    Run through a demonstration of how to use the GymAware API.
    """
    arg_parser = argparse.ArgumentParser(
        description='Test the Kinetic GymAware API'
    )
    arg_parser.add_argument(
        'accountID',
        type=str,
        help='Find your account ID under settings > API'
    )
    arg_parser.add_argument(
        'accountToken',
        type=str,
        help='Create new account tokens at settings > API'
    )
    group = arg_parser.add_mutually_exclusive_group()
    group.add_argument(
        '--url',
        help='Print the url suitable for curl.',
        action='store_true',
    )
    group.add_argument(
        '--athlete',
        help='Enter the firstName, lastName of your athlete',
        nargs=2,
        metavar=('FIRST_NAME', 'LAST_NAME')
    )
    group.add_argument(
        '--demo-athletes',
        help='Fetch athletes from the server and print them out.',
        action='store_true'
    )
    group.add_argument(
        '--list-staff',
        help='Fetch the staff accounts.',
        action='store_true'
    )
    group.add_argument(
        '--list-squads',
        help='Fetch the squad accounts.',
        type=str,
        metavar='STAFF_REFERENCE'
    )
    group.add_argument(
        '--demo-velocities',
        help='Fetch set summaries from the server and print velocity zones.',
        action='store_true'
    )
    group.add_argument(
        '--demo-leaderboard',
        help='Fetch set data from the server and show the best power results.',
        action='store_true'
    )
    group.add_argument(
        '--demo-reps',
        help='Fetch the max rep for each set for a given analysis.',
        type=str,
        metavar='ANALYSIS_LABEL'
    )
    arg_parser.add_argument(
        '--athlete-reference',
        help='Athlete Reference to filter results by.',
        type=str,
    )
    group.add_argument(
        '--refresh',
        help='Invalidate current credentials and ask for a new token.',
        action='store_true'
    )
    group.add_argument(
        '--analysis',
        help='Fetch the analysis options.',
        action='store_true'
    )
    group.add_argument(
        '--exercises',
        help='Fetch the exercise options.',
        action='store_true'
    )
    args = arg_parser.parse_args()
    authentication = (
          args.accountID,
          args.accountToken
    )
    try:
        no_results = True
        if args.athlete:
            print(
                json.dumps(
                    add_athlete(authentication, args.athlete),
                    sort_keys=True,
                    indent=4
                )
            )
            no_results = False
        elif args.url:
            print(server_url(
                args.accountID,
                args.accountToken
            ))
            no_results = False
        elif args.demo_athletes:
            print('Athletes')
            for athlete in athletes_from_server(authentication):
                athlete['archived'] = 'Archived' if athlete['deleted'] else ''
                print('"{firstName}", "{lastName}" {reference} {archived}'.format(**athlete))
                no_results = False
        elif args.demo_velocities:
            for zone, results in velocity_zone_results(authentication, args.athlete_reference):
                print(zone)
                for result in results:
                    print("\t{}: {} ({})".format(*result))
                    no_results = False
        elif args.demo_leaderboard:
            print('Best Power')
            for user, best in sorted_best_powers(authentication):
                print("{1}: {0}".format(user, best))
                no_results = False
        elif args.analysis:
            for label in analysis_by_label(authentication):
                print(label)
                no_results = False
        elif args.exercises:
            for label in exercises(authentication):
                print(label)
                no_results = False
        elif args.refresh:
            response = refresh_credentials_from_server(authentication)
            print("New Credentials")
            print(response)
            no_results = False
        elif args.demo_reps:
            analysis_label = args.demo_reps
            analysis_types = analysis_by_label(authentication)
            if analysis_label in analysis_types:
                analysis = analysis_types[analysis_label]
            else:
                analysis = next(iter(analysis_types.values()))
                print("{} not found. Showing {}".format(analysis_label, analysis['label']))
            print('Rep Stats : {}'.format(analysis['label']))
            for (user, recorded), best_rep in rep_stats(authentication, analysis, args.athlete_reference):
                print("{0}, {1}: {2}".format(user, recorded, best_rep))
                no_results = False
        elif args.list_staff:
            print('Staff')
            for staff in staff_from_server(authentication):
                print(f'{staff["staffReference"]} {staff["userID"]}')
                no_results = False
        elif args.list_squads:
            print('Squads')
            for squad in squads_from_server(authentication, args.list_squads):
                print(f'{squad["squadReference"]} {squad["name"]}')
                no_results = False
        elif args.squad:
            print(args.squad)
        else:
            arg_parser.print_help()
            no_results = False
        if no_results:
            print(NO_DATA)
    except ConnectionError as error:
        print("The GymAware server returned an exception.")
        print(error)


if __name__ == '__main__':
    print_demo()
