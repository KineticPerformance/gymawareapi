import click
from example import server, GYMAWARE_URL
import json


def get_list(auth, end_point, params):
    with server.stream(
        'GET',
        GYMAWARE_URL + end_point,
        auth=auth,
        params=params,
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            yield json.loads(line)


@click.group()
def cli():
    pass


@cli.command()
@click.argument('account')
@click.argument('token')
@click.option('--name', prompt='Squad Name',)
@click.option('--staff_reference', prompt='Target Staff References',)
@click.option('--athlete_references', prompt='Athlete References',
              help='List of athlete references.')
def add_squad(account, token, name, staff_reference, athlete_references):
    athlete_references = [ref.strip() for ref in athlete_references.split(',')]
    with server.stream(
        'POST',
        GYMAWARE_URL + 'squad',
        auth=(
              account,
              token
        ),
        params={
            'name': name,
            'staffReference': staff_reference,
            'athleteReferences': athlete_references,
        }
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            click.echo(json.loads(line))


@cli.command()
@click.argument('account')
@click.argument('token')
@click.option('--squad_reference', prompt='Target Squad References',)
@click.option('--name', prompt='Squad Name',)
@click.option('--active', prompt='Active/Download (0 or 1)',)
@click.option('--athlete_references', prompt='Athlete References',
              help='List of athlete references.', required=False, default='')
def edit_squad(account, token, name, active, squad_reference, athlete_references=None):
    if athlete_references:
        athlete_references = [ref.strip() for ref in athlete_references.split(',')]
    else:
        athlete_references = []
    with server.stream(
        'PUT',
        GYMAWARE_URL + 'squad',
        auth=(
              account,
              token
        ),
        params={
            'name': name,
            'isActive': active,
            'squadReference': squad_reference,
            'athleteReferences': athlete_references,
        }
    ) as stream:
        stream.raise_for_status()
        for line in stream.iter_lines():
            click.echo(json.loads(line))


@cli.command()
@click.argument('account')
@click.argument('token')
@click.option('--staff_reference', prompt='Target Staff References',)
def list_squads(account, token, staff_reference):
    for line in get_list(
        auth=(
              account,
              token
        ),
        end_point='squad',
        params={
            'staffReference': staff_reference,
        }
    ):
        click.echo(line)


@cli.command()
@click.argument('account')
@click.argument('token')
def list_staff(account, token):
    for line in get_list(
        auth=(
            account,
            token
        ),
        end_point='staff',
        params={}
    ):
        click.echo(line)


if __name__ == '__main__':
    cli()
