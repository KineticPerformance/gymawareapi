GymAware Cloud API EXAMPLE
==========================


Introduction
------------

Kinetic Performance Technology PTY LTD produces the GymAware Cloud platform to support our GymAware powertool.

The PowerTool collects data into sessions belonging to athletes. The athlete performs a protocol when collecting data which we categorise as exercises.

To access the api you must have an active GymAware Cloud account and set up a API token.

Documentation
-------------

The documentation lives at https://gymaware.zendesk.com/hc/en-us/articles/360001396875-API-Integration

Installation
------------

First install the dependencies

pip install -r requirements.txt

Clone this repository and run

python example.py APPLICATION_ID API_TOKEN --demo-athletes
